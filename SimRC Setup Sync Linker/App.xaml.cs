﻿using System.Windows;
using Prism.Ioc;
using SetupSyncLinker.Services;
using SetupSyncLinker.Views;

namespace SetupSyncLinker
{
    public partial class App
    {
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<ISettingsService, SettingsService>();
            containerRegistry.RegisterSingleton<ILinkService, LinkService>();
        }

        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }
    }
}
