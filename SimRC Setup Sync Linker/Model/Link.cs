﻿using System.IO;
using SetupSyncLinker.Util;

namespace SetupSyncLinker.Model
{
    public class Link
    {
        public string SetupFolderName { get; set; }
        public string DriveFolderName { get; set; }

        public bool Exists(Settings settings)
        {
            return Directory.Exists(GetFulliRacingPath(settings));
        }

        public void Create(Settings settings)
        {
            GetFulliRacingPath(settings).CreateSymbolicLinkTo(GetFullDrivePath(settings));
        }

        public void Delete(Settings settings)
        {
            GetFulliRacingPath(settings).DeleteSymbolicLink();
        }

        // ReSharper disable once IdentifierTypo
        private string GetFulliRacingPath(Settings settings)
        {
            return Path.Combine(settings.iRacingSetupsFolderPath, SetupFolderName, settings.LinkFolderName);
        }

        private string GetFullDrivePath(Settings settings)
        {
            return Path.Combine(settings.OneDriveSetupsFolderPath, DriveFolderName);
        }
    }
}
