﻿namespace SetupSyncLinker.Model
{
    public class Settings
    {
        // ReSharper disable once InconsistentNaming
        public string iRacingSetupsFolderPath { get; set; }
        public string OneDriveSetupsFolderPath { get; set; }
        public string LinkFolderName { get; set; }
    }
}
