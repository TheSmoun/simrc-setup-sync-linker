﻿using System.IO;
using Newtonsoft.Json;

namespace SetupSyncLinker.Services
{
    internal abstract class JsonServiceBase
    {
        protected virtual JsonSerializer Serializer => new JsonSerializer();

        protected void Serialize<T>(string path, T t) where T : class
        {
            using (var file = File.CreateText(path))
            {
                Serializer.Serialize(file, t);
            }
        }

        protected T Deserialize<T>(string path) where T : class
        {
            using (var file = File.OpenText(path))
            {
                return (T)Serializer.Deserialize(file, typeof(T));
            }
        }
    }
}
