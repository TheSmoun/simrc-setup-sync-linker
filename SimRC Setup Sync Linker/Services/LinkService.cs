﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SetupSyncLinker.Model;
using SetupSyncLinker.Util;

namespace SetupSyncLinker.Services
{
    public interface ILinkService
    {
        string FilePath { get; }

        List<Link> LoadLinks();
        bool Exists(Link link);
        bool Create(Link link);
        bool Delete(Link link);
    }

    internal class LinkService : JsonServiceBase, ILinkService
    {
        public string FilePath { get; } = Path.Combine(Assembly.GetExecutingAssembly().GetDirectoryPath(), "links.json");

        private readonly Settings _settings;

        public LinkService(ISettingsService settingsService)
        {
            _settings = settingsService.Load();
        }

        public List<Link> LoadLinks()
        {
            return Deserialize<List<Link>>(FilePath);
        }

        public bool Exists(Link link)
        {
            return link.Exists(_settings);
        }

        public bool Create(Link link)
        {
            link.Create(_settings);
            return Exists(link);
        }

        public bool Delete(Link link)
        {
            link.Delete(_settings);
            return !Exists(link);
        }
    }
}
