﻿using System;
using System.IO;
using System.Reflection;
using SetupSyncLinker.Model;
using SetupSyncLinker.Util;

namespace SetupSyncLinker.Services
{
    public interface ISettingsService
    {
        Settings Load();
        void Save(Settings settings);
    }

    internal class SettingsService : JsonServiceBase, ISettingsService
    {
        private static readonly string FilePath = Path.Combine(Assembly.GetExecutingAssembly().GetDirectoryPath(), "settings.json");

        private Settings _loadedSettings;

        public Settings Load()
        {
            return _loadedSettings ?? (_loadedSettings = !File.Exists(FilePath) ? CreateDefaultSettings() : FillDefaultProperties(Deserialize<Settings>(FilePath)));
        }

        public void Save(Settings settings)
        {
            Serialize(FilePath, settings);
        }

        private Settings CreateDefaultSettings()
        {
            var settings = FillDefaultProperties(new Settings());
            Save(settings);
            return settings;
        }

        private static Settings FillDefaultProperties(Settings settings)
        {
            var userPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            if (string.IsNullOrWhiteSpace(settings.iRacingSetupsFolderPath))
                settings.iRacingSetupsFolderPath = Path.Combine(userPath, "Documents", "iRacing", "setups");

            if (string.IsNullOrWhiteSpace(settings.OneDriveSetupsFolderPath))
                settings.OneDriveSetupsFolderPath = Path.Combine(userPath, "OneDrive", "SimRC", "Setups");

            if (string.IsNullOrWhiteSpace(settings.LinkFolderName))
                settings.LinkFolderName = "SimRC";

            return settings;
        }
    }
}
