﻿using System;
using System.IO;
using System.Reflection;

namespace SetupSyncLinker.Util
{
    internal static class AssemblyUtil
    {
        internal static string GetDirectoryPath(this Assembly assembly)
        {
            var codeBase = assembly.CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }
    }
}
