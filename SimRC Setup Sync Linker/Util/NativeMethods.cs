﻿using System.Runtime.InteropServices;

namespace SetupSyncLinker.Util
{
    internal static class NativeMethods
    {
        internal enum SymbolicLink
        {
            File = 0,
            Folder = 1
        }

        [DllImport("kernel32.dll")]
        internal static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, SymbolicLink dwFlags);
    }
}
