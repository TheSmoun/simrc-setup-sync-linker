﻿using System.IO;

namespace SetupSyncLinker.Util
{
    internal static class SymbolicLinkUtil
    {
        internal static bool CreateSymbolicLinkTo(this string sourcePath, string targetPath)
        {
            return NativeMethods.CreateSymbolicLink(sourcePath, targetPath, NativeMethods.SymbolicLink.Folder);
        }

        internal static void DeleteSymbolicLink(this string sourcePath)
        {
            if (Directory.Exists(sourcePath))
                Directory.Delete(sourcePath);
        }
    }
}
