﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using SetupSyncLinker.Model;
using SetupSyncLinker.Services;

namespace SetupSyncLinker.ViewModels
{
    public class LinkViewModel : BindableBase
    {
        public Link Link { get; }
        public ICommand CreateCommand { get; }
        public ICommand DeleteCommand { get; }

        public bool NotExists => !Exists;

        private bool _exists;
        public bool Exists
        {
            get => _exists;
            set
            {
                if (SetProperty(ref _exists, value))
                    RaisePropertyChanged(nameof(NotExists));
            }
        }

        private readonly ILinkService _linkService;

        public LinkViewModel(Link link, ILinkService linkService)
        {
            Link = link;
            _linkService = linkService;
            Exists = _linkService.Exists(Link);

            CreateCommand = new DelegateCommand(Create);
            DeleteCommand = new DelegateCommand(Delete);
        }

        internal void Create()
        {
            Exists = _linkService.Create(Link);
        }

        internal void Delete()
        {
            Exists = !_linkService.Delete(Link);
        }
    }
}
