﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using Prism.Commands;
using SetupSyncLinker.Services;

namespace SetupSyncLinker.ViewModels
{
    public class LinksViewModel
    {
        public ObservableCollection<LinkViewModel> Links { get; }
        public ICommand CreateAllCommand { get; }
        public ICommand DeleteAllCommand { get; }
        public ICommand ImportFileCommand { get; }

        private readonly ILinkService _linkService;

        public LinksViewModel(ILinkService linkService)
        {
            _linkService = linkService;

            Links = new ObservableCollection<LinkViewModel>();
            Refresh();

            CreateAllCommand = new DelegateCommand(CreateAll);
            DeleteAllCommand = new DelegateCommand(DeleteAll);
            ImportFileCommand = new DelegateCommand(ImportFile);
        }

        internal void Refresh()
        {
            Links.Clear();
            Links.AddRange(_linkService.LoadLinks().Select(l => new LinkViewModel(l, _linkService)));
        }

        private void CreateAll()
        {
            foreach (var link in Links.Where(l => l.NotExists))
            {
                link.Create();
            }
        }

        private void DeleteAll()
        {
            foreach (var link in Links.Where(l => l.Exists))
            {
                link.Delete();
            }
        }

        private void ImportFile()
        {
            var dialog = new OpenFileDialog {Multiselect = false, Filter = "JSON Files|*.json"};
            if (!dialog.ShowDialog().GetValueOrDefault())
                return;

            var path = dialog.FileName;
            try
            {
                File.Copy(path, _linkService.FilePath, true);
                Refresh();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error importing file." + Environment.NewLine + Environment.NewLine + e.Message);
            }
        }
    }
}
