﻿using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;
using SetupSyncLinker.Model;
using SetupSyncLinker.Services;

namespace SetupSyncLinker.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private Settings _settings;
        public Settings Settings
        {
            get => _settings;
            set => SetProperty(ref _settings, value);
        }

        public LinksViewModel Links { get; }

        public ICommand SaveiRacingFolderPathCommand { get; }
        public ICommand SaveOneDriveFolderPathCommand { get; }
        public ICommand SaveLinkFolderNameCommand { get; }

        private readonly ISettingsService _settingsService;

        public MainWindowViewModel(ISettingsService settingsService, ILinkService linkService)
        {
            _settingsService = settingsService;

            Settings = _settingsService.Load();

            SaveiRacingFolderPathCommand = new DelegateCommand<string>(OnSaveiRacingFolderPath);
            SaveOneDriveFolderPathCommand = new DelegateCommand<string>(OnSaveOneDriveFolderPathCommand);
            SaveLinkFolderNameCommand = new DelegateCommand<string>(OnSaveLinkFolderName);

            Links = new LinksViewModel(linkService);
        }

        private void OnSaveiRacingFolderPath(string path)
        {
            _settings.iRacingSetupsFolderPath = path;
            OnSettingsChanged();
        }

        private void OnSaveOneDriveFolderPathCommand(string path)
        {
            _settings.OneDriveSetupsFolderPath = path;
            OnSettingsChanged();
        }

        private void OnSaveLinkFolderName(string name)
        {
            _settings.LinkFolderName = name;
            OnSettingsChanged();
        }

        private void OnSettingsChanged()
        {
            _settingsService.Save(Settings);
            Links.Refresh();
            RaisePropertyChanged(nameof(Settings));
        }
    }
}
