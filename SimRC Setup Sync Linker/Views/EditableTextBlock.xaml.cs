﻿using System.Windows;
using System.Windows.Input;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace SetupSyncLinker.Views
{
    public partial class EditableTextBlock
    {
        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            nameof(Text), typeof(string), typeof(EditableTextBlock), new PropertyMetadata(OnTextChanged));

        public string Text
        {
            get => (string) GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static readonly DependencyProperty TextInternalProperty = DependencyProperty.Register(
            nameof(TextInternal), typeof(string), typeof(EditableTextBlock), new PropertyMetadata(default(string)));

        public string TextInternal
        {
            get => (string) GetValue(TextInternalProperty);
            set => SetValue(TextInternalProperty, value);
        }

        public static readonly DependencyProperty PathProperty = DependencyProperty.Register(
            nameof(Path), typeof(bool), typeof(EditableTextBlock), new PropertyMetadata(default(bool)));

        public bool Path
        {
            get => (bool) GetValue(PathProperty);
            set => SetValue(PathProperty, value);
        }

        public static readonly DependencyProperty EditingProperty = DependencyProperty.Register(
            nameof(Editing), typeof(bool), typeof(EditableTextBlock), new PropertyMetadata(default(bool)));

        public bool Editing
        {
            get => (bool) GetValue(EditingProperty);
            set => SetValue(EditingProperty, value);
        }

        public static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register(
            nameof(SaveCommand), typeof(ICommand), typeof(EditableTextBlock), new PropertyMetadata(default(ICommand)));

        public ICommand SaveCommand
        {
            get => (ICommand) GetValue(SaveCommandProperty);
            set => SetValue(SaveCommandProperty, value);
        }

        public EditableTextBlock()
        {
            InitializeComponent();
        }

        private void OnEdit(object sender, RoutedEventArgs e)
        {
            Editing = true;
        }

        private void OnSearch(object sender, RoutedEventArgs e)
        {
            using (var test = new CommonOpenFileDialog {IsFolderPicker = true})
            {
                if (test.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    TextInternal = test.FileName;
                }
            }
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            if (SaveCommand == null || !SaveCommand.CanExecute(TextInternal))
                return;

            SaveCommand.Execute(TextInternal);
            Editing = false;
        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            OnTextChanged();
            Editing = false;
        }

        private void OnTextChanged()
        {
            TextInternal = Text;
        }

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as EditableTextBlock)?.OnTextChanged();
        }
    }
}
